package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	private int fichas;
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int pFichas)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		fichas= pFichas;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public int registrarJugadaAleatoria()
	{

		int col = (int)(Math.random()*(tablero[0].length-1) + 1);
		return col;
		//TODO
	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean termino = false;
		boolean registro = false;
		for (int i = tablero.length -1; i > 0 && !registro; i--) {
			
			if(tablero[i][col].equals("___")){
				tablero[i][col] = "_"+jugadores.get(turno).darSimbolo() +"_";
				termino = terminar(i, col);
				registro = true;
				
			}
		}
		if( turno == jugadores.size()-1){
			turno=0;
		}
		else{
			turno++;
		}
		atacante = jugadores.get(turno).darNombre();
		//TODO
		return termino;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		String jugada = tablero[fil][col];
		boolean termino = false;
		//Columna 
		int contador = 0;
		for (int i = 0; i + fil < tablero.length ; i++) {
			if( tablero[fil + i][col].equals(jugada)){
				contador ++;
			}
			else{
				contador=0;
			}
			if( contador == fichas){
				termino = true;
			}
		}


		//Fila
		contador = 0;
		for (int i = 0; i < tablero[0].length ; i++) {
			if( tablero[fil][i].equals(jugada)){
				contador ++;
			}
			else{
				contador=0;
			}
			if( contador == fichas){
				termino = true;
			}
		}
		// columna superior izquierda 

		contador = 0;
		for (int i = 0; fil -i >=0 && col -i >=0 ; i++) {
			if( tablero[fil-i][col-i].equals(jugada)){
				contador ++;
			}
			else{
				contador=0;
			}
			if( contador == fichas){
				termino = true;
			}
		}
		// columna inferior izquierda 

		contador = 0;
		for (int i = 0; fil + i < tablero.length && col -i >=0 ; i++) {
			if( tablero[fil+i][col-i].equals(jugada)){
				contador ++;
			}
			else{
				contador=0;
			}
			if( contador == fichas){
				termino = true;
			}
		}
		//Columna superior derecha

		contador = 0;
		for (int i = 0; fil -i >=0 && col + i < tablero[0].length; i++) {
			if( tablero[fil-i][col+i].equals(jugada)){
				contador ++;
			}
			else{
				contador=0;
			}
			if( contador == fichas){
				termino = true;
			}
		}
		//COlumna inferior drecha 
		contador = 0;
		for (int i = 0; fil +i < tablero.length && col +i< tablero[0].length ; i++) {
			if( tablero[fil+i][col+i].equals(jugada)){
				contador ++;
			}
			else{
				contador=0;
			}
			if( contador == fichas){
				termino = true;
			}
		}
		//TODO
		return termino ;
	}



}
