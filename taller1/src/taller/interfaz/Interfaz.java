package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		System.out.println("Ingrese el número de jugadores");
		sc= new Scanner (System.in);
		int num =Integer.parseInt(sc.next());
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		for (int i = 0; i < num; i++) {
			System.out.println("Ingrese el nombre del juagdor "+ i +1 +"y el simbolo que desea usar:");
			String nombre = sc.next();
			String simbolo = sc.next();
			Jugador jugador = new Jugador(nombre, simbolo);
			jugadores.add(jugador);
		}

		System.out.println("Ingrese el número de filas y columnas del tamano del tablero");
		int num2 = Integer.parseInt(sc.next());

		if( num2 < 4){
			System.out.println("El tablero debe ser minimo de 4x4, ingrese un tamano nuevamente");
			num2 = Integer.parseInt(sc.next());
		}
		System.out.println("Ingrese el número de fichas de la linea para ganar el juego");
		int fichas = Integer.parseInt(sc.next());

		juego = new LineaCuatro(jugadores, num2, num2, fichas);
		imprimirTablero();
		juego();

		//TODO
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		boolean termino = false;
		String nombre = " ";
		while(!termino){

			nombre = juego.darAtacante();
			sc= new Scanner (System.in);
			System.out.println("Turno del jugador"+ nombre + ".Ingrese la columna");
			int num =Integer.parseInt(sc.next());
			termino = juego.registrarJugada(num-1);
			imprimirTablero();
		}
		System.out.println("Juego terminado.El ganador es:" + nombre);

		//TODO
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		sc= new Scanner (System.in);
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		System.out.println("Ingrese el nombre del jugador y el simbolo que desea usar:");
		String nombre = sc.next();
		String simbolo = sc.next();
		Jugador jugador = new Jugador(nombre, simbolo);
		Jugador maquina = new Jugador("Maquina", "+");
		jugadores.add(jugador);
		jugadores.add(maquina);

		System.out.println("Ingrese el número de filas y columnas del tamano del tablero");
		int num2 = Integer.parseInt(sc.next());

		if( num2 < 4){
			System.out.println("El tablero debe ser minimo de 4x4, ingrese un tamano nuevamente");
			num2 = Integer.parseInt(sc.next());
		}
		System.out.println("Ingrese el número de fichas de la linea para ganar el juego");
		int fichas = Integer.parseInt(sc.next());

		juego = new LineaCuatro(jugadores, num2, num2, fichas);
		imprimirTablero();
		juegoMaquina();
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		boolean termino = false;
		String nombre = " ";
		while(!termino){
			nombre = juego.darAtacante();
			sc= new Scanner (System.in);
			if( nombre.equals("Maquina")){
				System.out.println("Turno de la maquina");
				int num = juego.registrarJugadaAleatoria();
				termino = juego.registrarJugada(num);
				imprimirTablero();
			}
			else{
				System.out.println("Turno del jugador"+ nombre + ".Ingrese la columna");
				int num =Integer.parseInt(sc.next());
				termino = juego.registrarJugada(num-1);
				imprimirTablero();
			}
		}
		System.out.println("Juego terminado.El ganador es:" + nombre);

		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		for(int i=0;i<juego.darTablero().length;i++)
		{

			for(int j=0;j<juego.darTablero()[0].length;j++)
			{
				System.out.print("|"+juego.darTablero()[i][j]+"|");
			}
			System.out.println("");
		}
		//TODO
	}
}
